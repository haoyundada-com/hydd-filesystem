<?php
// +----------------------------------------------------------------------
// | HaoYunDaDa [ 欢迎使用好运哒哒 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2024-2025 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: TanHuiXing <53297668@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace haoyundada\facade;

use haoyundada\Facade;
use haoyundada\filesystem\Driver;

/**
 * Class Filesystem
 * @package haoyundada\facade
 * @mixin \haoyundada\Filesystem
 * @method static Driver disk(string $name = null) , null|string
 * @method static mixed getConfig(null|string $name = null, mixed $default = null) 获取缓存配置
 * @method static array getDiskConfig(string $disk, null $name = null, null $default = null) 获取磁盘配置
 * @method static string|null getDefaultDriver() 默认驱动
 */
class Filesystem extends Facade
{
    protected static function getFacadeClass()
    {
        return \haoyundada\Filesystem::class;
    }
}
